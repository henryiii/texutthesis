UTThesis
--------


This class was designed to match the University of Texas at Austin Guidelines, 2013.
Perfect correspondence is not guaranteed, but should be close. It was used for several
successful thesis documents in 2015 and 2016, and is continuing to be used.

This is a class for building a UT thesis. It uses standard latex unpacking procedures
for a single dtx file. Running latex on the dtx file will produce all needed files.
Running tex on the dtx file will only produce necessary files (not documentation).
Place at least the .cls in your thesis folder, or in the standard location for
installed tex files.

Included files
==============

``readme.rst``
  The file you are reading.

``samplethesis.tex``
  A sample thesis file, to demonstrate usage.

``utthesis.dtx``
  The file that generates the other files. Run this through pdflatex.

Generated files
===============

These files are available in the downloads section, or you can run `pdflatex utthesis.dtx`.

``utthesis.cls``
  The class file that you'll use to set the class for your thesis.

``utthesis.pdf``
  A detailed help manual for the class.

``utthesis.cwl``
  A file for TexStudio to provide highlighting for the new commands.

``README.txt``
  A readme for the class

Running with Docker
===================

To build and run your thesis genration through Docker anywhere (Mac, Linux, Windows), you simply
need to have Docker installed. You'll need to start the Docker deamon/service on your system. There is no
official latex build, but the tianon one is complete and very simple. You just need to run
a command that looks like this from this directory (modifications required if you are using Windows CMD)::

    docker run --volume=`pwd`:/texutthesis --workdir="/texutthesis" tianon/latex pdflatex utthesis.dtx

You might want to run that twice if you are producing the output pdf.
To create the sample PDF::

    docker run --volume=`pwd`:/texutthesis --workdir="/texutthesis" tianon/latex latexmk -pdf samplethesis.tex

Of course, this is Docker, so you can also enter the container and run the commands yourself::

    docker run -it --volume=`pwd`:/texutthesis --workdir="/texutthesis" tianon/latex /bin/bash

This repository is also build by Bitbucket's Pipelines feature.

