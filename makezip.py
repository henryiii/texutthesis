#!/usr/bin/env python3

from plumbum import FG
from plumbum.cmd import pdflatex
import zipfile

FILES = [
    'samplethesis.pdf',
    'samplethesis.tex',
    'utthesis.cls',
    'utthesis.cwl',
    'utthesis.dtx',
    'utthesis.pdf',
    ]

pdflatex['utthesis.dtx'] & FG
pdflatex['samplethesis.tex'] & FG

with zipfile.ZipFile('texutthesis.zip', 'w', zipfile.ZIP_DEFLATED) as myzip:
    for file in FILES:
        myzip.write(file,file)
